'use strict';

const assert = require('assert');
const bcrypt = require('bcrypt');
const crypto = require('mz/crypto');
const notp = require('notp');
const uuid = require('uuid-1345');

class Transaction {
	constructor(t) {
		this._t = t;
		this._msgs = [];
	}

	async network_create({name}) {
		assert.equal(typeof name, 'string');
		const id = (await this._t.one('INSERT INTO networks(name) VALUES($1) RETURNING network_id', name)).network_id;
		return id;
	}

	async network_get_name(network_id) {
		assert(uuid.check(network_id));
		return (await this._t.one('SELECT name FROM networks WHERE network_id = $1', network_id)).name;
	}

	async network_set_name(network_id, name) {
		assert(uuid.check(network_id));
		await this._t.one('UPDATE networks SET name = $2 WHERE network_id = $1 RETURNING network_id', [network_id, name]);
	}

	async network_exists(network_id) {
		assert(uuid.check(network_id));
		return (await this._t.one('SELECT EXISTS(SELECT 1 FROM networks WHERE network_id = $1)', network_id)).exists;
	}

	async network_delete(network_id) {
		assert(uuid.check(network_id));
		await this._t.none('DELETE FROM networks WHERE network_id = $1', network_id);
	}

	async tinc_assign_static({network_id, hostname, port}) {
		assert(uuid.check(network_id));
		await this._t.none('INSERT INTO network_tinc_servers(network_id, hostname, port, static, rsa_public_key) VALUES($1, $2, $3, TRUE, NULL)', [network_id, hostname, port]);
	}

	async tinc_get_servers(network_id) {
		assert(uuid.check(network_id));
		return await this._t.any('SELECT * FROM network_tinc_servers WHERE network_id = $1', network_id);
	}

	async tinc_get_servers_by_hostname(hostname) {
		return await this._t.any('SELECT * FROM network_tinc_servers WHERE hostname = $1', hostname);
	};

	// gets all tinc servers that the user owns (i.e., the tinc servers of the hostnames that that user owns)
	async tinc_get_servers_by_userid(user_id) {
		assert(uuid.check(user_id));
		return await this._t.any(
			`SELECT network_tinc_servers.*
			FROM network_tinc_servers, user_has_hostname
			WHERE user_has_hostname.user_id = $1
				AND user_has_hostname.hostname = network_tinc_servers.hostname`,
			user_id
		);
	};

	async tinc_update({network_id, hostname, port}, {rsa_public_key}) {
		assert(uuid.check(network_id));
		await this._t.none('UPDATE network_tinc_servers SET rsa_public_key = $1 WHERE network_id = $2 AND hostname = $3 AND port = $4', [rsa_public_key, network_id, hostname, port]);
	}

	async user_create({username}) {
		assert(typeof username === 'string', 'user_create() requires username of type string');
		return (await this._t.one('INSERT INTO users(username) VALUES($1) RETURNING user_id', username)).user_id;
	}

	async user_get_name(user_id) {
		assert(uuid.check(user_id));
		return (await this._t.one('SELECT username FROM users WHERE user_id = $1', user_id)).username;
	}

	// returns null if there is no user with that username
	async user_get_uid_from_username(username) {
		assert(typeof username === 'string');
		const ret = await this._t.oneOrNone('SELECT user_id FROM users WHERE username = $1', username);
		return ret ? ret.user_id : null;
	}

	async user_set_password(user_id, password) {
		assert(uuid.check(user_id));
		const hash = await bcrypt.hash(password, 12);
		await this._t.one('UPDATE users SET bcrypt_password = $1 WHERE user_id = $2 RETURNING user_id', [hash, user_id]);
	}

	async user_validate_password(user_id, password) {
		assert(uuid.check(user_id) || user_id === null);
		const hash_obj = await this._t.oneOrNone('SELECT bcrypt_password FROM users WHERE user_id = $1', user_id);
		let hash = '';
		if (hash_obj && hash_obj.bcrypt_password)
			hash = hash_obj.bcrypt_password;
		return await bcrypt.compare(password, hash);
	}

	async user_totp_generate_key(user_id) {
		assert(uuid.check(user_id));
		const key = (await crypto.randomBytes(16)).toString('base64');
		await this._t.one('UPDATE users SET totp_secret = $1 WHERE user_id = $2 RETURNING user_id', [key, user_id]);
		return key;
	}

	async user_totp_verify_token(user_id, token) {
		assert(uuid.check(user_id));
		const key = (await this._t.one('SELECT totp_secret FROM users WHERE user_id = $1', user_id)).totp_secret;
		return !!notp.totp.verify(token, key);
	}

	async user_has_role(user_id, role_id) {
		assert(uuid.check(user_id));
		assert(uuid.check(role_id));
		const has = (await this._t.one('SELECT EXISTS(SELECT 1 FROM user_has_role WHERE user_id = $1 AND role_id = $2)', [user_id, role_id])).exists;
		return has;
	}

	async user_give_role(user_id, role_id) {
		assert(uuid.check(user_id));
		assert(uuid.check(role_id));
		await this._t.none('INSERT INTO user_has_role(user_id, role_id) VALUES($1, $2) ON CONFLICT DO NOTHING', [user_id, role_id]);
	}

	async user_has_hostname(user_id, hostname) {
		assert(uuid.check(user_id));
		assert.equal(typeof hostname, 'string');
		const has = await this._t.one('SELECT EXISTS(SELECT 1 FROM user_has_hostname WHERE user_id = $1 AND hostname = $2)', [user_id, hostname]);
		return has.exists;
	}

	async user_give_hostname(user_id, hostname) {
		await this._t.none('INSERT INTO user_has_hostname(user_id, hostname) VALUES($1, $2) ON CONFLICT DO NOTHING', [user_id, hostname]);
	}

	async role_create({name, network_id}) {
		assert.equal(typeof name, 'string');
		assert(uuid.check(network_id));
		return (await this._t.one(
			'INSERT INTO roles(name, network_id) VALUES($1, $2) RETURNING role_id', [name, network_id]
		)).role_id;
	}

	async role_assign_ip_range(role_id, {ip_begin, ip_end, masklen = 32}) {
		assert(uuid.check(role_id));
		await this._t.none(
			'INSERT INTO role_ip4(role_id, ips, masklen) VALUES($1, ip4r($2, $3), $4)',
			[role_id, ip_begin, ip_end, masklen]
		);
	}

	async role_instantiate(role_id, {rsa_public_key}) {
		assert(uuid.check(role_id));
		const network_id = (await this._t.one('SELECT network_id FROM roles WHERE role_id = $1', role_id)).network_id;
		const ip = (await this._t.one(
			`SELECT set_masklen(alpha.ip::cidr::inet, alpha.masklen)::text AS ip
			FROM (
				SELECT generate_series(lower(ips)::bigint, upper(ips)::bigint)::ip4 AS ip, masklen
				FROM role_ip4 WHERE role_id = $1
			) alpha
			LEFT JOIN nodes
				ON alpha.ip = nodes.ip4_address AND nodes.network_id = $2
				WHERE nodes.ip4_address IS NULL
			LIMIT 1`,
			[role_id, network_id]
		)).ip;
		const id = (await this._t.one(
			`INSERT INTO nodes(network_id, role_id, rsa_public_key, ip4_address)
			VALUES($1, $2, $3, set_masklen($4, 32))
			RETURNING node_id`,
			[network_id, role_id, rsa_public_key, ip]
		)).node_id;

		return {node_id: id, ips: [ip]};
	}

	async node_get_info(node_id) {
		assert(uuid.check(node_id));
		return await this._t.one('SELECT rsa_public_key FROM nodes WHERE node_id = $1', node_id);
	}

	async node_update(node_id, {rsa_public_key}) {
		assert(uuid.check(node_id));
		await this._t.one(
			`UPDATE nodes SET rsa_public_key = $1, last_seen = NOW()
			WHERE node_id = $2 RETURNING node_id`,
			[rsa_public_key, node_id]
		);
	}

	async node_update_last_seen(node_id) {
		assert(uuid.check(node_id));
		await this._t.one(
			'UPDATE nodes SET last_seen = NOW() WHERE node_id = $1 RETURNING node_id',
			node_id
		);
	}
};

// db is pg-promise Database object, with a connection to an already set-up database
module.exports = async db => {
	const db_util = require('./db_util')(db);

	let ret = {};

	let listeners = new Map();

	let is_done = false;
	let listen;
	listen = await db.connect({direct: true});
	listen.client.on('notification', data => {
		const d = JSON.parse(data.payload);
		switch (data.channel) {
			case 'network_change':
				if (listeners.has(d.network_id))
					for (const f of listeners.get(d.network_id))
						f(d);
				break;
		}
	});
	listen.client.on('error', () => {});
	listen.client.on('end', async () => {
		listen = null;
		if (!is_done) {
			while (true) {
				try {
					listen = await db.connect({direct:true});
				} catch(e) {
					await new Promise(resolve => setTimeout(resolve, 100));
					continue;
				}
				break;
			}
			await listen.none('LISTEN "network_change"');
			for (const [network_id, list] of listeners.entries())
				for (const f of list)
					f({network_id});

		}
	});
	await listen.none('LISTEN "network_change"');

	ret.listen_network_change = (network_id, f) => {
		assert(uuid.check(network_id));
		if (!listeners.has(network_id))
			listeners.set(network_id, []);
		listeners.get(network_id).push(f);
	};

	ret.transact = async f => {
		let trans;
		const ret = await db_util.transactorSRW(db, async t => {
			trans = new Transaction(t);
			return await f(trans);
		});
		return ret;
	};

	ret.transactRO = async f => {
		let trans;
		const ret = await db_util.transactorSROD(db, async t => {
			trans = new Transaction(t);
			return await f(trans);
		});
		return ret;
	};

	ret.done = async () => {
		is_done = true;
		if (listen) {
			listen.done();
			const p = new Promise(resolve => listen.client.on('end', resolve));
			listen = null;
			await p;
		}
	};

	Object.freeze(ret);
	return ret;
};
