'use strict';
const assert = require('assert');
const child_process = require('mz/child_process');
const pg_promise = require('pg-promise')();

let pgp_db;
let db;

describe('db_util', function() {
	before(async function() {
		this.timeout(10000);
		// for some reason pg-promise doesn't work without -t
		// -t uses a TCP port instead of a UNIX socket
		// -w5 to garbage collect after 5 seconds
		const uri = await child_process.execFile('sudo', [`PATH=${process.env.PATH}`, '-u', 'nobody', (await child_process.execFile('which', ['pg_tmp']))[0].trim(), '-w', '5', '-t']);
		assert.equal(typeof uri[0], 'string');
		pgp_db = pg_promise(uri[0]);
		db = await require('../db_util')(pg_promise);

		// make sure the database is up before we start using it in tests
		await pgp_db.one('SELECT version()');
	});

	describe('.transactorSROD()', async function() {
		it('SERIALIZABLE READ ONLY DEFERRABLE', async function() {
			const f = async t => {
				assert.equal('on', (await t.one('SHOW transaction_deferrable')).transaction_deferrable);
				assert.equal('serializable', (await t.one('SHOW transaction_isolation')).transaction_isolation);
				assert.equal('on', (await t.one('SHOW transaction_read_only')).transaction_read_only);
				return 'a value';
			};
			Object.freeze(f);
			const res = await db.transactorSROD(pgp_db, f);
			assert.equal(res, 'a value');
		});
	});

	describe('.transactorSRW()', async function() {
		it('SERIALIZABLE', async function() {
			const f = async t => {
				assert.equal('off', (await t.one('SHOW transaction_deferrable')).transaction_deferrable);
				assert.equal('serializable', (await t.one('SHOW transaction_isolation')).transaction_isolation);
				assert.equal('off', (await t.one('SHOW transaction_read_only')).transaction_read_only);
				return 'a value';
			};
			Object.freeze(f);
			const res = await db.transactorSRW(pgp_db, f);
			assert.equal(res, 'a value');
		});
	});

	describe('.transactor()', function() {
		it('SERIALIZABLE READ ONLY DEFERRABLE', async function() {
			const f = async t => {
				assert.equal('on', (await t.one('SHOW transaction_deferrable')).transaction_deferrable);
				assert.equal('serializable', (await t.one('SHOW transaction_isolation')).transaction_isolation);
				assert.equal('on', (await t.one('SHOW transaction_read_only')).transaction_read_only);
				return 'a value';
			};
			f.txMode = db.tmSerializableRODeferrable;
			Object.freeze(f);
			const res = await db.transactor(pgp_db, f);
			assert.equal(res, 'a value');
		});

		it('SERIALIZABLE', async function() {
			const f = async t => {
				assert.equal('off', (await t.one('SHOW transaction_deferrable')).transaction_deferrable);
				assert.equal('serializable', (await t.one('SHOW transaction_isolation')).transaction_isolation);
				assert.equal('off', (await t.one('SHOW transaction_read_only')).transaction_read_only);
				return 'a value';
			};
			f.txMode = db.tmSerializableRW;
			Object.freeze(f);
			const res = await db.transactor(pgp_db, f);
			assert.equal(res, 'a value');
		});

		it('exception on no txMode', async function() {
			const f = async t => { };
			try {
				await db.transactor(pgp_db, f);
			} catch(e) {
				return;
			}

			assert(false);
		});

		it('exception on bad txMode', async function() {
			const f = async t => { };
			f.txMode = 'this is a string not a transaction';
			try {
				await db.transactor(pgp_db, f);
			} catch(e) {
				return;
			}

			assert(false);
		});

		it('force retry', async function() {
			// this example can be found from https://www.postgresql.org/docs/9.6/static/transaction-iso.html
			// we are purposefully creating two transactions and ordering them so a serializable order isn't possible
			// and they are retried automatically be transactor
			await pgp_db.none('CREATE TABLE mytab (class INT, value INT)');
			await pgp_db.none('INSERT INTO mytab VALUES(1,10),(1,20),(2,100),(2,200)');

			let do1, do2, do3;
			const step1 = new Promise(resolve => do1 = resolve);
			const step2 = new Promise(resolve => do2 = resolve);
			const step3 = new Promise(resolve => do3 = resolve);

			let runs = 0;

			const A = async t => {
				runs++;
				await step1;
				const sum = await t.one('SELECT SUM(value) AS sum FROM mytab WHERE class = 1');
				await t.none('INSERT INTO mytab VALUES(2, $1)', sum.sum);
				do2();
				await step3;
				return sum.sum;
			};
			A.txMode = db.tmSerializableRW;
			const B = async t => {
				runs++;
				await step2;
				const sum = await t.one('SELECT SUM(value) AS sum FROM mytab WHERE class = 2');
				await t.none('INSERT INTO mytab VALUES(1, $1)', sum.sum);
				do3();
				return sum.sum;
			}
			B.txMode = db.tmSerializableRW;

			const run = Promise.all([db.transactor(pgp_db, A), db.transactor(pgp_db, B)]);
			do1();
			let res = await run;
			assert(runs === 3);
			assert( (res[0] === '30' && res[1] === '330') || (res[0] === '330' && res[1] === '300') );
		});
	});
});
