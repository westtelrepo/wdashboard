'use strict';
const child_process = require('mz/child_process');
const path = require('path');
const pg_promise = require('pg-promise')();
const url = require('url');

const schema = new pg_promise.QueryFile(path.resolve('schema.pgsql'));

module.exports = class {
	async before() {
		const uri = await child_process.execFile('sudo', [`PATH=${process.env.PATH}`, '-u', 'westtel', (await child_process.execFile('which', ['pg_tmp']))[0].trim(), '-w', '60', '-t']);
		// the original connection, superuser access to default database
		this._super_user_db = pg_promise(uri[0]);
		// access for user 'wpn' to database 'wpn'
		this._db_connection = pg_promise(url.format(Object.assign(url.parse(uri[0]), {auth:'wpn', pathname:'/wpn'})));
		// super-user access to the database 'wpn'
		this._super_user_db_wpn = pg_promise(url.format(Object.assign(url.parse(uri[0]), {pathname: '/wpn'})));
		const super_user_db_wpn_template = pg_promise(url.format(Object.assign(url.parse(uri[0]), {pathname: '/wpn_template'})));
		const db_wpn_template = pg_promise(url.format(Object.assign(url.parse(uri[0]), {auth: 'wpn', pathname: '/wpn_template'})));

		await this._super_user_db.none('CREATE USER wpn');
		await this._super_user_db.none(
			`CREATE DATABASE wpn_template WITH OWNER wpn
			ENCODING 'UTF8' LC_COLLATE 'C' LC_CTYPE 'C' TEMPLATE 'template0'`
		);
		// load schema into template
		await super_user_db_wpn_template.none('CREATE EXTENSION "uuid-ossp"');
		await super_user_db_wpn_template.none('CREATE EXTENSION "btree_gist"');
		await super_user_db_wpn_template.none('CREATE EXTENSION "ip4r"');
		await db_wpn_template.none(schema);

		// make template unconnectable
		await this._super_user_db.none("UPDATE pg_database SET datallowconn = FALSE where datname = 'wpn_template'");
		await this._super_user_db.any("SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = 'wpn_template'");
	}

	// returns the pg-promise Database for use by the application
	async beforeEach() {
		await this._super_user_db.none("CREATE DATABASE wpn WITH OWNER wpn TEMPLATE 'wpn_template'");
		return this._db_connection;
	}

	async afterEach() {
		await this._super_user_db.none('UPDATE pg_database SET datallowconn = FALSE WHERE datname = \'wpn\'');
		await this.kill_connections();
		await this._super_user_db.none('DROP DATABASE wpn');
	}

	async after() {
		pg_promise.end();
	}

	async kill_connections() {
		await this._super_user_db.any('SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = \'wpn\'');
	}
};
