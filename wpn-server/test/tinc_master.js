'use strict';

const assert = require('assert');
const child_process = require('mz/child_process');
const sleep = require('es7-sleep');
const tinc_master = require('../tinc_master');
const tinc_node = require('../tinc_node');
const uuid = require('uuid-1345');

describe('tinc_master', function() {
	it('constructor', async function() {
		new tinc_master();
	});

	it('.stop()', async function() {
		const master = new tinc_master();
		await master.stop();
	});

	it('ping through master', async function() {
		this.slow(2500);
		this.timeout(10000);
		const master = new tinc_master();
		const t1 = new tinc_node();
		const t2 = new tinc_node();
		t1.bindToAddress = ['127.0.0.2'];
		t2.bindToAddress = ['127.0.0.3'];
		t1.ips = ['25.0.0.1/24'];
		t2.ips = ['25.0.0.2/24'];
		t1.port = 656;
		t2.port = 656;

		await t1.generateKeyPair(512);
		await t2.generateKeyPair(512);

		const n = uuid.v4();

		const info = await master.update(n, {
			hosts: [{
				name: t1.name,
				publicKey: t1.publicKey,
			},{
				name: t2.name,
				publicKey: t2.publicKey,
			}],
			bits: 512,
		});

		t1.hosts = [{
			name: t1.name,
			publicKey: t1.publicKey,
			subnets: ['25.0.0.1/32'],
		},{
			name: info.name,
			publicKey: info.publicKey,
			addresses: ['localhost'],
		}];

		t2.hosts = [{
			name: t2.name,
			publicKey: t2.publicKey,
			subnets: ['25.0.0.2/32'],
		},{
			name: info.name,
			publicKey: info.publicKey,
			addresses: ['localhost'],
		}];

		t1.connectTo = [info.name];
		t2.connectTo = [info.name];

		t1.netns = 'master_t1_' + uuid.v4();
		t2.netns = 'master_t2_' + uuid.v4();

		await child_process.exec(`ip netns add ${t1.netns}`);
		await child_process.exec(`ip netns add ${t2.netns}`);
		await child_process.exec(`ip netns exec ${t1.netns} ip link set lo up`);
		await child_process.exec(`ip netns exec ${t2.netns} ip link set lo up`);

		await Promise.all([t1.start(), t2.start()]);

		try {
			await child_process.exec(`ip netns exec ${t1.netns} ping -c1 25.0.0.2`);
		} finally {
			await Promise.all([master.stop(), t1.stop(), t2.stop()]);
			await child_process.exec(`ip netns del ${t1.netns}`);
			await child_process.exec(`ip netns del ${t2.netns}`);
		}
	});

	it('TCP connection survives key regen', async function() {
		this.slow(10000);
		this.timeout(40000);
		const master = new tinc_master();
		const t1 = new tinc_node();
		const t2 = new tinc_node();
		t1.bindToAddress = ['127.0.0.2'];
		t2.bindToAddress = ['127.0.0.3'];
		t1.ips = ['25.0.0.1/24'];
		t2.ips = ['25.0.0.2/24'];
		t1.port = 656;
		t2.port = 656;

		await t1.generateKeyPair(512);
		await t2.generateKeyPair(512);

		const n = uuid.v4();

		const info = await master.update(n, {
			hosts: [{
				name: t1.name,
				publicKey: t1.publicKey,
			},{
				name: t2.name,
				publicKey: t2.publicKey,
			}],
			bits: 512,
		});

		t1.hosts = [{
			name: t1.name,
			publicKey: t1.publicKey,
			subnets: ['25.0.0.1/32'],
		},{
			name: info.name,
			publicKey: info.publicKey,
			addresses: ['localhost'],
		}];

		t2.hosts = [{
			name: t2.name,
			publicKey: t2.publicKey,
			subnets: ['25.0.0.2/32'],
		},{
			name: info.name,
			publicKey: info.publicKey,
			addresses: ['localhost'],
		}];

		t1.connectTo = [info.name];
		t2.connectTo = [info.name];

		t1.netns = 'master_t1_' + uuid.v4();
		t2.netns = 'master_t2_' + uuid.v4();

		await child_process.exec(`ip netns add ${t1.netns}`);
		await child_process.exec(`ip netns add ${t2.netns}`);
		await child_process.exec(`ip netns exec ${t1.netns} ip link set lo up`);
		await child_process.exec(`ip netns exec ${t2.netns} ip link set lo up`);

		await Promise.all([t1.start(), t2.start()]);

		try {
			await child_process.exec(`ip netns exec ${t1.netns} ping -c1 25.0.0.2`);
			const listener = child_process.exec(`ip netns exec ${t2.netns} nc -l -p 3000`);
			const writer = child_process.spawn('ip', ['netns', 'exec', t1.netns, 'nc', '-N', '25.0.0.2', '3000']);

			await t1.generateKeyPair(512);
			await t2.generateKeyPair(512);

			await new Promise(resolve => writer.stdin.write('hello, ', resolve));

			const info = await master.update(n, {
				hosts: [{
					name: t1.name,
					publicKey: t1.publicKey,
				},{
					name: t2.name,
					publicKey: t2.publicKey,
				}],
				bits: 512,
			});

			t1.hosts = [{
				name: t1.name,
				publicKey: t1.publicKey,
				subnets: ['25.0.0.1/32'],
			},{
				name: info.name,
				publicKey: info.publicKey,
				addresses: ['localhost'],
			}];

			t2.hosts = [{
				name: t2.name,
				publicKey: t2.publicKey,
				subnets: ['25.0.0.2/32'],
			},{
				name: info.name,
				publicKey: info.publicKey,
				addresses: ['localhost'],
			}];

			t1.connectTo = [info.name];
			t2.connectTo = [info.name];

			await Promise.all([t1.stop(), t2.stop()]);
			await Promise.all([t1.start(), t2.start()]);
			await new Promise(resolve => writer.stdin.end('world', resolve));

			await child_process.exec(`ip netns exec ${t1.netns} ping -c1 25.0.0.2`);
			assert.equal('hello, world', (await listener)[0]);
		} finally {
			await Promise.all([master.stop(), t1.stop(), t2.stop()]);
			await child_process.exec(`ip netns del ${t1.netns}`);
			await child_process.exec(`ip netns del ${t2.netns}`);
		}
	});
});
