'use strict';

const WebSocket = require('ws');
const assert = require('assert');
const uuid = require('uuid-1345');

const db_ = Symbol('wpn_server.db_');
const ws_ = Symbol('wpn_server.ws_');

class Connection {
	constructor({
		db,
		ws,
	} = {}) {
		this.db_ = db;
		this.ws_ = ws;
		this.uid_ = null;

		Object.seal(this);

		this.ws_.on('message', this.handle_.bind(this));
	}

	async handle_(data) {
		try {
			const msg = JSON.parse(data);
			switch (msg._c) {
				case 'PING':
					if (msg._r > 0)
						this.ws_.send(JSON.stringify({_c:'OK', _r: -msg._r}));
					break;
				case 'LOGIN':
					await this.handle_login(msg);
					break;
				case 'WHOAMI':
					this.ws_.send(JSON.stringify({_c:'OK', _r: -msg._r, uid:this.uid_}));
					break;
				case 'CONNECT':
					if (this.uid_)
						await this.handle_connect(msg);
					else
						this.ws_.send(JSON.stringify({_c: 'ERROR', _r: -msg._r, e: 'EACCESS'}));
					break;
				case 'LIST_MY_TINC':
					if (this.uid_)
						await this.handle_list_my_tinc(msg);
					else
						this.ws_.send(JSON.stringify({_c: 'ERROR', _r: -msg._r, e: 'EACCESS'}));
					break;
				default:
					assert(false);
			}
		} catch(e) {
			console.error('Error in connection', e);
			this.ws_.close();
		}
	}

	async handle_login(msg) {
		switch (msg.method) {
			case 'password':
				const pass = await this.db_.transactRO(async t => {
					let uid;
					if (msg.username && !msg.uid)
						uid = await t.user_get_uid_from_username(msg.username);
					else
						uid = msg.uid;
					return {
						pass: await t.user_validate_password(uid, msg.password),
						uid: uid
					};
				});
				if (pass.pass) {
					this.ws_.send(JSON.stringify({_c: 'OK', _r: -msg._r}));
					this.uid_ = pass.uid;
				} else
					this.ws_.send(JSON.stringify({_c: 'ERROR', _r: -msg._r}));
				break;
			default:
				assert(false);
		}
	}

	async handle_connect(msg) {
		assert(uuid.check(msg.role_id));
		assert(typeof msg.rsa_public_key === 'string');
		const ret = await this.db_.transact(async t => {
			const has = await t.user_has_role(this.uid_, msg.role_id);
			if (!has)
				return null;
			return await t.role_instantiate(msg.role_id, {rsa_public_key: msg.rsa_public_key});
		});
		if (ret)
			this.ws_.send(JSON.stringify({_c:'OK', _r: -msg._r, node_id: ret.node_id, ips: ret.ips}));
		else
			this.ws_.send(JSON.stringify({_c:'ERROR', _r: -msg._r, e: 'EACCESS'}));
	}

	async handle_list_my_tinc(msg) {
		const ret = await this.db_.transactRO(async t => {
			return await t.tinc_get_servers_by_hostname('example.com');
		});
		this.ws_.send(JSON.stringify({_c:'OK', _r: -msg._r, list: ret}));
	}
};

module.exports = Object.freeze(class {
	constructor({
		db, // from db.js
		port = 8080,
	} = {}) {
		this[db_] = db;
		this[ws_] = new WebSocket.Server({port});
		this[ws_].on('connection', ws => new Connection({db: this[db_], ws}));
	}

	async close() {
		// TODO: kill all clients
		await new Promise(resolve => this[ws_].close(resolve));
		this[ws_] = null;
	}
});
